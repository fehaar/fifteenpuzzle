﻿using FifteenPuzzle.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FifteenPuzzle
{
    public partial class MainForm : Form
    {
        private Button[] _buttons;
        private GameState _gameState;

        public MainForm()
        {
            InitializeComponent();
            _buttons = new Button[]
            {
                button0, button1, button2, button3,
                button4, button5, button6, button7,
                button8, button9, button10, button11,
                button12, button13, button14, button15
            };

            for (int i = 0; i < _buttons.Length; i++)
            {
                // Set up the tag of the button to be the index of it
                var btn = _buttons[i];
                btn.Tag = i;
                // Hook up the new game state
                btn.Click += Button_Click;
            }

            SetGameState(new GameState());
        }

        private void SetGameState(GameState gameState)
        {
            _gameState = gameState;
            var values = _gameState.Values.ToArray();
            for (int i = 0; i < _buttons.Length; i++)
            {
                var btn = _buttons[i];
                btn.Text = (values[i] == 0) ? "" : values[i].ToString();
                btn.Enabled = _gameState.IsValidMove(i) && !_gameState.IsWinningState();
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            // The index of the button is the same as its tag
            var index = (int)(sender as Button).Tag;
            var result = _gameState.Move(index);
            if (result.IsValid)
            {
                SetGameState(result.GameState);
                if (result.GameState.IsWinningState())
                {
                    WonGame();
                }
            }
        }

        private void WonGame()
        {
            // TODO: Do something exciting here
        }
    }
}
