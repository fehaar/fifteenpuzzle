﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FifteenPuzzle.Core.Test
{
    /// <summary>
    /// This test suite contains invalid game moves
    /// </summary>
    public class InvalidGameMoves : xSpec
    {
        private byte[] _knownValidGameState = new byte[] {
                        3, 5, 8, 10,
                        1, 6, 12, 2,
                        9, 4, 0, 11,
                        13, 14, 7, 15
                    };

        [Fact]
        public void AnInvalidMoveWillReturnTheOriginalGameState()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            var moveResult = default(MoveResult);
            Where("we apply an invalid move from position 1",
                () =>
                {
                    moveResult = gs.Move(1);
                });

            Expect("an invalid result with the old game state",
                () =>
                {
                    moveResult.IsValid.Should().BeFalse();
                    moveResult.GameState.Should().Be(gs);
                });
        }

        [Fact]
        public void AMoveThatIsNotWithinRangeWillThrowAnException()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            Expect("an exception if we try to move from an unknown position",
                () =>
                {
                    gs.Invoking(g => g.Move(-1)).ShouldThrow<InvalidOperationException>();
                    gs.Invoking(g => g.Move(16)).ShouldThrow<InvalidOperationException>();
                });
        }
    }
}
