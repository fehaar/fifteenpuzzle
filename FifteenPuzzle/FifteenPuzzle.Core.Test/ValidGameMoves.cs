﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FifteenPuzzle.Core.Test
{
    /// <summary>
    /// This test suite contains valid game moves
    /// </summary>
    public class ValidGameMoves : xSpec
    {
        private byte[] _knownValidGameState = new byte[] {
                        3, 5, 8, 10,
                        1, 6, 12, 2,
                        9, 4, 0, 11,
                        13, 14, 7, 15
                    };

        [Fact]
        public void AssertingTheValidMoves()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            Expect("there to be four valid moves",
                () =>
                {
                    gs.IsValidMove(6).Should().BeTrue();
                    gs.IsValidMove(9).Should().BeTrue();
                    gs.IsValidMove(11).Should().BeTrue();
                    gs.IsValidMove(14).Should().BeTrue();
                });
        }

        [Fact]
        public void AValidMoveCanBeAppliedToTheGameState()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            var moveResult = default(MoveResult);
            Where("we apply a move from position 6",
                () =>
                {
                    moveResult = gs.Move(6);
                });

            Expect("a valid result with a new game state",
                () =>
                {
                    moveResult.IsValid.Should().BeTrue();
                    moveResult.GameState.Should().NotBe(gs);
                });
        }

        [Fact]
        public void WeCanDoADownMove()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            var moveResult = default(MoveResult);
            Where("we apply a move from position 6",
                () =>
                {
                    moveResult = gs.Move(6);
                });

            Expect("the number on position 6 to have swapped with 0",
                () =>
                {
                    var resultValues = moveResult.GameState.Values.ToArray();
                    resultValues[6].Should().Be(0);
                    resultValues[10].Should().Be(_knownValidGameState[6]);
                });
        }

        [Fact]
        public void WeCanDoAnUpMove()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            var moveResult = default(MoveResult);
            Where("we apply a move from position 14",
                () =>
                {
                    moveResult = gs.Move(14);
                });

            Expect("the number on position 14 to have swapped with 0",
                () =>
                {
                    var resultValues = moveResult.GameState.Values.ToArray();
                    resultValues[14].Should().Be(0);
                    resultValues[10].Should().Be(_knownValidGameState[14]);
                });
        }

        [Fact]
        public void WeCanDoALeftMove()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            var moveResult = default(MoveResult);
            Where("we apply a move from position 9",
                () =>
                {
                    moveResult = gs.Move(9);
                });

            Expect("the number on position 9 to have swapped with 0",
                () =>
                {
                    var resultValues = moveResult.GameState.Values.ToArray();
                    resultValues[9].Should().Be(0);
                    resultValues[10].Should().Be(_knownValidGameState[9]);
                });
        }

        [Fact]
        public void WeCanDoARightMove()
        {
            var gs = default(GameState);
            Given("a game state with known values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            var moveResult = default(MoveResult);
            Where("we apply a move from position 11",
                () =>
                {
                    moveResult = gs.Move(11);
                });

            Expect("the number on position 11 to have swapped with 0",
                () =>
                {
                    var resultValues = moveResult.GameState.Values.ToArray();
                    resultValues[11].Should().Be(0);
                    resultValues[10].Should().Be(_knownValidGameState[11]);
                });
        }
    }
}
