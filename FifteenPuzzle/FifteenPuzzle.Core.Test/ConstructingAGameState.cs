﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FifteenPuzzle.Core.Test
{
    public class ConstructingAGameState : xSpec
    {
        [Fact]
        public void CreatingAGameState()
        {
            // Setup
            var gs = default(GameState);
            Given("a new game state", 
                () =>
                {
                    gs = new GameState();
                });

            var values = default(byte[]);
            Where("we get the values",
                () =>
                {
                    values = gs.Values.ToArray();
                });
        
            Expect("that we have the values from 0-15 present",
                () =>
                {
                    values.Should().HaveCount(16);
                    for (byte i = 0; i < 16; i++)
                    {
                        // The values from 0 - 15 should all be present
                        values.Should().Contain(i);
                    }
                });
        }

        [Fact]
        public void AGameStateWillHaveRandomValues()
        {
            var gs1 = default(GameState);
            var gs2 = default(GameState);
            Given("two game states",
                () =>
                {
                    gs1 = new GameState();
                    gs2 = new GameState();
                });

            var values1 = default(byte[]);
            var values2 = default(byte[]);
            Where("we get the values",
                () =>
                {
                    values1 = gs1.Values.ToArray();
                    values2 = gs2.Values.ToArray();
                });

            Expect("that the values should not be the same",
                () =>
                {
                    values1.Should().NotEqual(values2);
                });
        }

        #region Methods only used for tests
        [Fact]
        public void WeCanAssignValuesToTheGameState()
        {
            var values = default(byte[]);
            Given("a set of values that are valid for a game state",
                () =>
                {
                    values = Enumerable.Range(0, 16)
                                .Select(n => (byte)n)
                                .ToArray();
                });

            var gs = default(GameState);
            That("we use to create a game state with",
                () =>
                {
                    gs = new GameState(values);
                });

            var gsValues = default(byte[]);
            Expect("that the game state will contain these values in order",
                () =>
                {
                    gsValues = gs.Values.ToArray();
                    gsValues.Should().ContainInOrder(values);
                });
        }
        #endregion Methods only used for tests
    }
}
