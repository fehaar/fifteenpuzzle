﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FifteenPuzzle.Core.Test
{
    public class InvalidEdgeMoves : xSpec
    {
        [Fact]
        public void InvalidEdgeMoveOnTheLeftSide()
        {
            var gs = default(GameState);
            Given("a game state with the index on the left side",
                () =>
                {
                    gs = new GameState(new byte[] {
                        3, 5, 8, 10,
                        0, 6, 12, 2,
                        9, 4, 1, 11,
                        13, 14, 7, 15
                    });
                });

            Expect("the move from 3 to be invalid",
                () =>
                {
                    gs.IsValidMove(3).Should().BeFalse();
                });
        }

        [Fact]
        public void InvalidEdgeMoveOnTheRightSide()
        {
            var gs = default(GameState);
            Given("a game state with the index on the right side",
                () =>
                {
                    gs = new GameState(new byte[] {
                        3, 5, 8, 10,
                        2, 6, 12, 0,
                        9, 4, 1, 11,
                        13, 14, 7, 15
                    });
                });

            Expect("the move from 8 to be invalid",
                () =>
                {
                    gs.IsValidMove(8).Should().BeFalse();
                });
        }
    }
}
