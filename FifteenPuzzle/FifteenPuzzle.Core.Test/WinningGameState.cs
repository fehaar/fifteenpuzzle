﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FifteenPuzzle.Core.Test
{
    public class WinningGameState : xSpec
    {
        private byte[] _knownValidGameState = new byte[] {
                        3, 5, 8, 10,
                        1, 6, 12, 2,
                        9, 4, 0, 11,
                        13, 14, 7, 15
                    };
        private byte[] _knownWinningGameState = new byte[] {
                        1, 2, 3, 4,
                        5, 6, 7, 8,
                        9, 10, 11, 12,
                        13, 14, 15, 0
                    };

        [Fact]
        public void AGameStateWithNonOrderedValuesIsNotAWinningState()
        {
            var gs = default(GameState);
            Given("a gamestate with the known out of order values",
                () =>
                {
                    gs = new GameState(_knownValidGameState);
                });

            Expect("the gamestate not to be winning",
                () =>
                {
                    gs.IsWinningState().Should().BeFalse();
                });
        }

        [Fact]
        public void AGameStateWithOrderedValuesIsAWinningState()
        {
            var gs = default(GameState);
            Given("a gamestate with ordered values to be winning",
                () =>
                {
                    gs = new GameState(_knownWinningGameState);
                });

            Expect("the gamestate to be winning",
                () =>
                {
                    gs.IsWinningState().Should().BeTrue();
                });
        }
    }
}
