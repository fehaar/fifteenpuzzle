﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FifteenPuzzle.Core
{
    /// <summary>
    /// This is the Game state for the Fifteen puzzle.
    /// It is an immutable class containing the current state of the game.
    /// </summary>
    public class GameState
    {
        // This is used for creating new random game states
        private static Random rnd = new Random();

        public GameState()
        {
            _values = Enumerable.Range(0, 16)
                        .Select(n => (byte)n)
                        .OrderBy(_ => rnd.Next())
                        .ToArray();
            CacheEmptyIndex();
        }

        /// <summary>
        /// This constructor is used for tests to create 
        /// known game states.
        /// </summary>
        /// <param name="values"></param>
        internal GameState(byte[] values)
        {
            _values = values;
            CacheEmptyIndex();
        }

        private byte[] _values;
        /// <summary>
        /// These are the values of the game state. 
        /// It will contain the numbers 0-15 in any order.
        /// </summary>
        public IEnumerable<byte> Values
        {
            get
            {
                return _values;
            }
        }

        public bool IsWinningState()
        {
            if (_emptyIndex < _values.Length - 1)
            {
                return false;
            }
            for (int i = 0; i < _values.Length - 1; i++)
            {
                if (_values[i] != i + 1)
                {
                    return false;
                }
            }
            return true;
        }

        private int _emptyIndex;
        // Record the position of the empty index, as we use this to determine validity
        // of a move and swapping values
        private void CacheEmptyIndex()
        {
            // Find and cache index of the empty value
            for (int i = 0; i < _values.Length; i++)
            {
                if (_values[i] == 0)
                {
                    _emptyIndex = i;
                    break;
                }
            }
        }

        public MoveResult Move(int position)
        {
            var valid = IsValidMove(position);
            var gs = this;
            if (valid)
            {
                var newValues = _values.ToArray();
                newValues[_emptyIndex] = newValues[position];
                newValues[position] = 0;
                gs = new GameState(newValues);
            }
            return new MoveResult(valid, gs);
        }

        /// <summary>
        /// Is the current move a valid move.
        /// A valid move will be from a position that is adjecant to the empty value.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public bool IsValidMove(int position)
        {
            if (position < 0 || position > _values.Length - 1)
            {
                throw new InvalidOperationException($"You cannot move from an invalid position {position}");
            }
            return _emptyIndex == position + 4 
                || _emptyIndex == position - 4
                // The mod operator takes care of not wrapping in the sides.
                || (_emptyIndex == position - 1 && _emptyIndex % 4 != 3)
                || (_emptyIndex == position + 1 && _emptyIndex % 4 != 0);
        }
    }
}
