﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FifteenPuzzle.Core
{
    public class MoveResult
    {
        public MoveResult(bool isValid, GameState gameState)
        {
            IsValid = isValid;
            GameState = gameState;
        }

        public bool IsValid { get; private set; }
        public GameState GameState { get; private set; }
    }
}
