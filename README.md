# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a quick implementation of a classic fifteen puzzle.

The purpose is to show my coding style and method of solving a simple problem.

### Implementation details ###

The solution has three projects:

FifteenPuzzle
A Windows forms project that shows the game.

FifteenPuzzle.Core
A class library that holds the game state class and supporting logic.

FifteenPuzzle.Core.Test
Test classes for validating that the game state and game works.

In general everything in the game is centered around the immutable game state. The state holds the 16 bytes that make up the layout of the puzzle grid. When you make a move you get a new game state with the result of the state if the move is valid. If you do an invalid move, you will get the old game state back.

The UI will create a random game state and enable the buttons that comprise the valid moves. When a button is pressed the move is used to make a new game state hat will then be shown. Note that the victory state is present, but currently does nothing. So you can continue playing.

The tests are written in specification style and test the core functionality of the game. They are not meant to be exhaustive and test all edge cases.